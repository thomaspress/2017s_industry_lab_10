package ictgradschool.industry.lab10.Quiz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by tpre939 on 5/12/2017.
 */
public class QuestionOne {

    private void start(){

        ArrayList<String> list = new ArrayList<>();
        String[] stringers = new String[]{"The" , "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"};

        list.addAll(Arrays.asList(stringers));

        Collections.shuffle(list);

        for (String i : list) {
            System.out.println(i);
        }

    }

    public static void main(String[] args) {
        new QuestionOne().start();
    }
}
