package ictgradschool.industry.lab10.Quiz;

import java.io.*;
import java.util.Map;
import java.util.Scanner;
import java.util.HashMap;


/**
 * Created by tpre939 on 5/12/2017.
 */
public class QuestionTwo {

    private void start(){

        HashMap<String, Integer> hMap = new HashMap();

        File file = new File("weightlist.txt");
//        try (Scanner scanner = new Scanner(file).useDelimiter(",\\n")){
//            while (scanner.hasNext()){
//                hMap.put(scanner.next().trim(),Integer.parseInt(scanner.next().trim()));
//            }
//        }
//        catch (IOException e){
//            System.out.println("File not found" + e);
//        }
        try (BufferedReader bR = new BufferedReader(new FileReader(file))){
            String line = null;
            while ((line = bR.readLine()) != null ){
                String[] lineArr = line.split(",");
                hMap.put(lineArr[0],Integer.parseInt(lineArr[1]));
            }
        } catch (FileNotFoundException e){
            System.out.println("File not found " + e);
        }
        catch (IOException e){
            System.out.println("Uh Oh" + e);
        }



        for (Map.Entry<String,Integer> i: hMap.entrySet()){
            System.out.println(i);
        }

    }

    public static void main(String[] args) {
        new QuestionTwo().start();
    }
}
