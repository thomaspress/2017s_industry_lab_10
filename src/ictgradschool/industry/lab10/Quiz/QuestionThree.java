package ictgradschool.industry.lab10.Quiz;

/**
 * Created by tpre939 on 5/12/2017.
 */
public class QuestionThree {

    private void start(){

        System.out.println(reverseString("Test String"));
        System.out.println(power(2,2));

    }


    private int power(int x, int y){
        if (y == 1){
            return x;
        }
        return x*power(x, y-1);
    }



    private String reverseString(String s){
        if (s.length() == 0){
            return "";
        }
        String revString = reverseString(s.substring(1));
        return revString + s.charAt(0);
    }


    public static void main(String[] args) {
        new QuestionThree().start();
    }
}
